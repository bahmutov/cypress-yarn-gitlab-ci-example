# cypress-yarn-gitlab-ci-example
> Example installing NPM and Cypress dependencies using Yarn on GitLab CI

See [.gitlab-ci.yml](.gitlab-ci.yml) how to cache Yarn dependencies and Cypress binary on GitLab CI.

For more information, see [Cypress continuous integration guide](https://on.cypress.io/ci)
